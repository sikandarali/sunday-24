-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: v1-prod-sunday-24.cda4cc5d7cuk.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

#SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_collector_version`
--

DROP TABLE IF EXISTS `tbl_collector_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_collector_version` (
  `collector_version_id` int NOT NULL AUTO_INCREMENT,
  `version` varchar(127) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `stamp` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int DEFAULT '0',
  `file_size` varchar(50) DEFAULT NULL,
  `release_notes` longtext,
  `email_notes` longtext,
  `update_schedule` datetime DEFAULT NULL,
  `update_status` enum('OPEN','SCHEDULED','UPDATING','COMPLETED','FAILED') DEFAULT NULL,
  `include_release_notes` tinyint DEFAULT '0',
  PRIMARY KEY (`collector_version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_collector_version`
--

LOCK TABLES `tbl_collector_version` WRITE;
/*!40000 ALTER TABLE `tbl_collector_version` DISABLE KEYS */;
INSERT INTO `tbl_collector_version` VALUES (1,'1.2.8','Visibilityone_1.2.8_Beta.exe','2019-01-12 20:41:18',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(2,'1.2.9','Visibilityone_1.2.9_Beta.exe','2019-01-16 20:41:18',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(3,'1.3.3','VisibilityOne_1.3.3_Beta.exe','2019-03-01 05:58:23',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(4,'1.3.4','VisibilityOne_1.3.4_Beta.exe','2019-07-15 21:57:21',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(5,'1.4.0','VisibilityOne_Collector_1.6.0.exe','2019-09-04 13:44:00',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(6,'1.7.3','VisibilityOne_Collector-1.7.3.exe','2020-02-08 05:49:04',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(7,'1.7.4','VisibilityOne_Collector-1.7.4.exe','2020-02-27 06:46:01',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(8,'1.7.6','VisibilityOne_Collector-1.7.6.exe','2020-02-28 07:52:46',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(9,'1.7.8','VisibilityOne_Collector-1.7.8.exe','2020-03-04 22:02:54',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(10,'1.7.12','VisibilityOne_Collector-1.7.12.exe','2020-06-09 18:39:24',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(11,'2.0.2','VisibilityOne_Collector-2.0.2.exe','2020-06-18 07:45:46',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(12,'2.0.3','VisibilityOne_Collector-2.0.3.exe','2020-06-20 04:39:23',0,'227 MB',NULL,NULL,NULL,'COMPLETED',0),(13,'2.0.5','VisibilityOne_Collector-2.0.5.exe','2020-09-24 11:38:40',0,'238.1 MB',NULL,NULL,NULL,'COMPLETED',0),(14,'2.0.6','VisibilityOne_Collector-2.0.6.exe','2020-10-04 17:04:30',0,'64.9 MB',NULL,NULL,NULL,'COMPLETED',0),(15,'2.0.8','VisibilityOne_Collector-2.0.8.exe','2020-10-10 07:12:37',0,'63.42 MB',NULL,NULL,NULL,'COMPLETED',0),(16,'2.0.9','VisibilityOne_Collector-2.0.9.exe','2020-11-03 14:52:13',0,'63.42 MB',NULL,NULL,NULL,'COMPLETED',0),(17,'2.0.10','VisibilityOne_Collector-2.0.10.exe','2020-11-07 02:58:22',0,'63.42 MB','notes',NULL,'2020-11-24 14:50:00','COMPLETED',0),(18,'2.0.11','VisibilityOne_Collector-2.0.11.exe','2021-02-17 08:24:40',0,'63.42 MB','Modification on installer file',NULL,'2021-04-09 17:00:00','COMPLETED',0),(19,'2.0.12','VisibilityOne_Collector-2.0.12.exe','2021-04-29 03:53:25',0,'63.42 MB','Fix of VVX device discovery',NULL,'2021-05-05 00:00:00','COMPLETED',0),(20,'2.0.13','VisibilityOne_Collector-2.0.13.exe','2021-05-11 10:05:52',0,'63.42 MB','Implementation on Poly x series firmware update',NULL,'2021-06-27 15:30:00','COMPLETED',0),(21,'2.0.14','VisibilityOne_Collector-2.0.14.exe','2021-08-09 15:18:54',0,'63.42 MB','Fix on VVX devices restart issue',NULL,NULL,'COMPLETED',0),(22,'2.0.15','VisibilityOne_Collector-2.0.15.exe','2022-03-31 07:25:26',0,'61.9 MB','Fixed special character in password error',NULL,'2023-01-28 14:00:00','COMPLETED',0),(25,'2.1.2','VisibilityOne_Collector-2.1.2.exe','2023-02-12 00:00:00',0,'61.9 MB','Minor Bug Fixes',NULL,NULL,'COMPLETED',0),(26,'2.1.7','VisibilityOne_Collector-2.1.7.exe','2023-02-13 00:00:00',0,'61.9 MB','Minor Bug Fixes',NULL,NULL,'COMPLETED',0),(27,'2.1.9','VisibilityOne_Collector-2.1.9.exe','2023-05-03 00:00:00',0,'99.0 MB','Added support for Poly Trio Visual+',NULL,'2023-05-06 14:20:00','COMPLETED',0),(28,'2.1.10','VisibilityOne_Collector-2.1.10.exe','2023-05-12 00:00:00',0,'99.0 MB','Added support for Poly Trio Visual+',NULL,'2023-05-06 14:20:00','COMPLETED',0),(29,'2.1.11','VisibilityOne_Collector-2.1.11.exe','2023-05-19 00:00:00',0,'99.0 MB','Added support for Poly Trio Visual+',NULL,'2023-07-07 06:12:00','COMPLETED',0),(30,'2.1.12','VisibilityOne_Collector-2.1.12.exe','2023-08-10 00:00:00',0,'99.3 MB','Added support for Windows 2019 DC server.',NULL,NULL,'COMPLETED',0),(31,'2.1.13','VisibilityOne_Collector-2.1.13.exe','2023-09-22 00:00:00',1,'99.3 MB','Added support for Poly Welness Check',NULL,NULL,'OPEN',0);
/*!40000 ALTER TABLE `tbl_collector_version` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-24 11:15:48
