-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: v1-prod-sunday-24.cda4cc5d7cuk.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

#SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_tunnels`
--

DROP TABLE IF EXISTS `tbl_tunnels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_tunnels` (
  `tunnel_id` varchar(255) NOT NULL,
  `collector_id` int unsigned DEFAULT NULL,
  `ip_policy_id` varchar(100) NOT NULL,
  `tunnel_address` varchar(100) NOT NULL,
  `type` enum('http','tcp','tls') DEFAULT 'http',
  `status` enum('AVAILABLE','RUNNING','STOPPED') DEFAULT NULL,
  `started_time` datetime DEFAULT NULL,
  `stopped_time` datetime DEFAULT NULL,
  PRIMARY KEY (`tunnel_id`),
  UNIQUE KEY `tbl_tunnels_ip_policy_id_unique` (`ip_policy_id`),
  UNIQUE KEY `tbl_tunnels_tunnel_address_unique` (`tunnel_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tunnels`
--

LOCK TABLES `tbl_tunnels` WRITE;
/*!40000 ALTER TABLE `tbl_tunnels` DISABLE KEYS */;
INSERT INTO `tbl_tunnels` VALUES ('byfphlwu9p',NULL,'ipp_1iwK6YEYnsmtOQoMr10CuycgtF3','byfphlwu9p.linkinstantly.io','http','AVAILABLE',NULL,NULL),('hwpwuyv78j',NULL,'ipp_1iwK41ho1aflWrAp4mSthDqhfhp','hwpwuyv78j.linkinstantly.io','http','AVAILABLE',NULL,NULL),('pzbxgz1645',NULL,'ipp_1iwK9V0Hw2ogQnbJ1OQ0mohu5iq','pzbxgz1645.linkinstantly.io','http','AVAILABLE',NULL,NULL),('r832bz8rlk',NULL,'ipp_1iwJyAOCxWRg9RefInKKBQn5DUx','r832bz8rlk.linkinstantly.io','http','AVAILABLE',NULL,NULL),('whfuds8ub4',NULL,'ipp_1iwK1GFDTKSHO2MK9gMl7CN2mhJ','whfuds8ub4.linkinstantly.io','http','AVAILABLE',NULL,NULL);
/*!40000 ALTER TABLE `tbl_tunnels` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-24 11:12:55
