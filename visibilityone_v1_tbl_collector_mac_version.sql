-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: v1-prod-sunday-24.cda4cc5d7cuk.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

#SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_collector_mac_version`
--

DROP TABLE IF EXISTS `tbl_collector_mac_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_collector_mac_version` (
  `collector_version_id` int NOT NULL AUTO_INCREMENT,
  `version` varchar(127) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `stamp` datetime DEFAULT NULL,
  `active` int DEFAULT NULL,
  `file_size` varchar(50) DEFAULT NULL,
  `release_notes` longtext,
  `email_notes` longtext,
  `update_schedule` datetime DEFAULT NULL,
  `update_status` enum('OPEN','SCHEDULED','UPDATING','COMPLETED','FAILED') DEFAULT NULL,
  `include_release_notes` tinyint DEFAULT '0',
  PRIMARY KEY (`collector_version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_collector_mac_version`
--

LOCK TABLES `tbl_collector_mac_version` WRITE;
/*!40000 ALTER TABLE `tbl_collector_mac_version` DISABLE KEYS */;
INSERT INTO `tbl_collector_mac_version` VALUES (1,'1.0.0_mac','VSONECollector-1.0.0.dmg','2021-10-06 15:12:18',0,'112.6 MB ','Phase 1',NULL,NULL,'COMPLETED',1),(2,'2.0.1_mac','VSONECollector-2.0.1.dmg','2021-11-16 23:26:30',0,'117.4 MB','Phase 2 Iot',NULL,NULL,'COMPLETED',1),(3,'2.0.2_mac','VSOneCollector-2.0.2.pkg','2022-02-09 23:26:30',0,'107.4 MB','Fix issue',NULL,NULL,'COMPLETED',1),(4,'2.0.6_mac','VSONECollector-2.0.6.pkg','2022-08-16 23:26:30',0,'107.4 MB','Fix issue',NULL,NULL,'COMPLETED',1),(5,'2.0.7_mac','VSONECollector-2.0.7.pkg','2022-09-07 23:26:30',0,'107.4 MB','Logging Implementation',NULL,NULL,'COMPLETED',1),(6,'2.0.8_mac','VSONECollector-2.0.8.pkg','2022-09-23 23:26:30',1,'107.4 MB','Fix issue',NULL,NULL,'COMPLETED',1);
/*!40000 ALTER TABLE `tbl_collector_mac_version` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-24 11:12:54
