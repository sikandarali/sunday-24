-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: v1-prod-sunday-24.cda4cc5d7cuk.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

#SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_video_data_live`
--

DROP TABLE IF EXISTS `tbl_video_data_live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_video_data_live` (
  `video_data_live_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int unsigned DEFAULT NULL,
  `site_id` int unsigned DEFAULT NULL,
  `video_device_id` int unsigned DEFAULT NULL,
  `video_call_id` bigint unsigned DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `call_status` int DEFAULT NULL,
  `total_mbps` int DEFAULT NULL,
  `total_jitter` int DEFAULT NULL,
  `total_latency` int DEFAULT NULL,
  `total_packetloss` double DEFAULT NULL,
  `active_mics` int DEFAULT NULL,
  `total_mics` int DEFAULT NULL,
  `active_cameras` int DEFAULT NULL,
  `total_cameras` int DEFAULT NULL,
  `temprature` float DEFAULT NULL,
  `util_total_calls` int DEFAULT NULL,
  `util_total_usage` varchar(45) DEFAULT NULL,
  `util_adhoc` int DEFAULT NULL,
  `util_scheduled` int DEFAULT NULL,
  `util_failed` int DEFAULT NULL,
  `health` int DEFAULT NULL,
  `qos` int DEFAULT NULL,
  `scheduled_meetings` int DEFAULT NULL,
  `audioRx` json DEFAULT NULL,
  `audioTx` json DEFAULT NULL,
  `videoRx` json DEFAULT NULL,
  `videoTx` json DEFAULT NULL,
  `contentRx` json DEFAULT NULL,
  `contentTx` json DEFAULT NULL,
  `lastcall` varchar(255) DEFAULT NULL,
  `health_info` json DEFAULT NULL,
  `mute` tinyint DEFAULT '0',
  `temprature_text` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`video_data_live_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17512444 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_video_data_live`
--

LOCK TABLES `tbl_video_data_live` WRITE;
/*!40000 ALTER TABLE `tbl_video_data_live` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_video_data_live` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-24 11:12:56
